﻿require('settings')
require('mapping')
vim.cmd[[

if has('nvim')
  function! UpdateRemotePlugins(...)
    " Needed to refresh runtime files
    let &rtp=&rtp
    UpdateRemotePlugins
  endfunction
set encoding=utf8
set t_Co=256





"set guifont=Monaco\Font:h10

let g:material_style = 'palenight'
let g:neovide_transparency=0.9
let g:material_italic_comments = v:true
let g:material_italic_keywords = v:true
let g:material_italic_functions = v:true
let g:material_contrast = v:true
autocmd VimEnter * colorscheme deus
autocmd BufEnter * lua require'completion'.on_attach()
autocmd BufEnter * :COQnow --shut-up
"autocmd BufWritePre *.c Neoformat clangformat
"autocmd BufWritePost *.c   let errored =  system(join(["norminette", @%, "|","grep ", "Error:", "|",  "head -n 5" ], " ")) 
autocmd BufWritePost *.c   :AsyncRun!  "norminette" % | head -n 5 
"autocmd BufWritePost  *     :AsyncRun!  "make"
"autocmd BufWritePost *.h   :AsyncRun!  "norminette" % | head -n 5 
"autocmd BufWritePost *.c   :call asyncrun#run("/bin/result/bin/norminette", %)
"autocmd BufWritePost *.c  :cexp errored
"autocmd BufWritePost *.c  :cc
"autocmd BufWritePost *.c  let pos = system(join([ "~/bin/result/bin/norminette",@%, "|","awk", "'{if(/line:/)  print substr($4,1,length($4) - 1 )}'", "|",  "head -n 1" ], " "))
"autocmd BufWritePost *.c  let pos1 = system(join([ "~/bin/result/bin/norminette",@%, "|","awk", "'{if(/col:/)  print substr($4,1,length($4) - 1 )}'", "|",  "head -n 1" ], " "))
"autocmd BufWritePost *.c  :echo cursor(pos,pos1)
call plug#begin('~/.config/nvim/plugged')


Plug 'ms-jpq/coq_nvim'
Plug 'ms-jpq/coq.thirdparty'
Plug 'github/copilot.vim'
Plug 'hoob3rt/lualine.nvim'
Plug 'neovim/nvim-lsp'
Plug 'gelguy/wilder.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'skywind3000/asyncrun.vim'
Plug 'williamboman/nvim-lsp-installer'
Plug 'nvim-lua/plenary.nvim'
Plug 'ryanoasis/vim-devicons'
Plug 'marko-cerovac/material.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'numtostr/FTerm.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'sbdchd/neoformat'
Plug '42og/stdheader.vim'
Plug 'haorenW1025/diagnostic-nvim'
Plug 'haorenW1025/completion-nvim'
Plug 'nvim-lua/completion-nvim'
Plug 'glepnir/lspsaga.nvim'
Plug 'folke/lsp-colors.nvim'
Plug 'wakatime/vim-wakatime'
Plug 'nightsense/strawberry'
Plug 'ajmwagar/vim-deus'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp'
Plug 'hoob3rt/lualine.nvim'
Plug 'flazz/vim-colorschemes'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-treesitter/nvim-treesitter', {'branch' : '0.5-compat'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects', {'branch' : '0.5-compat'}
Plug 'ThePrimeagen/harpoon'
Plug 'ray-x/lsp_signature.nvim'
let g:lsp_settings = {'tsserver':{'whitelist': ['typescript', 'typescriptreact']}}
call plug#end()
call wilder#enable_cmdline_enter()
set wildcharm=<Tab>
cmap <expr> <Tab> wilder#in_context() ? wilder#next() : "\<Tab>"
cmap <expr> <S-Tab> wilder#in_context() ? wilder#previous() : "\<S-Tab>"
call wilder#set_option('modes', [':','/','?'])

let s:highlighters = [
        \ 
        \   wilder#highlighter_with_gradient ([ wilder#basic_highlighter() ]),
		\
        \ ]

let s:scale = ['#f4468f', '#fd4a85', '#ff507a', '#ff566f', '#ff5e63',
      \ '#ff6658', '#ff704e', '#ff7a45', '#ff843d', '#ff9036',
      \ '#f89b31', '#efa72f', '#e6b32e', '#dcbe30', '#d2c934',
      \ '#c8d43a', '#bfde43', '#b6e84e', '#aff05b']
let s:gradient = map(s:scale, {i, fg -> wilder#make_hl(
      \ 'WilderGradient' . i, 'Pmenu', [{}, {}, {'foreground': fg}]
      \ )})

call wilder#set_option('renderer', wilder#popupmenu_renderer(wilder#popupmenu_border_theme({
      \ 'highlights': {
      \ 'gradient': s:gradient,
      \   'accent': wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#83a598'}]),
      \ },
      \ 'highlighter':  s:highlighters,
      \   'left': [
      \     wilder#popupmenu_devicons(),
      \   ],
      \   'right': [
      \     ' ',
      \     wilder#popupmenu_scrollbar(),
      \   ],
      \ 'min_width': '35%',
      \ 'max_height': '30%',
      \ 'reverse': 1,
      \ 'border': 'rounded',
      \ })))
call wilder#project_root(['.git'])	
call wilder#set_option('pipeline', [
      \   wilder#branch(
      \     wilder#python_file_finder_pipeline({
		\
		\'file_command': ['fd',getcwd(),'--full-path','--type','f', '-E', '*.o' ], 
      \       'dir_command': ['fd','--full-path','-td'],
      \       'filters': ['fuzzy_filter', 'difflib_sorter'],
      \     }),
      \     wilder#cmdline_pipeline({
      \       'fuzzy': 1,
      \       'fuzzy_filter': wilder#vim_fuzzy_filter(),
      \ }),
      \     wilder#python_search_pipeline({
      \     }),
      \   ),
      \ ])
]]
 require('FTerm')

 require'nvim-treesitter.configs'.setup {
	 highlight = {  
	  enable = true
	},
	incrimental_selecion = {
	enable = true,
	 keymaps = {
      init_selection = "gnn",
      node_nncremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
	},
    indent = {
      enable = true
    },
textobjects = {
    select = {
      enable = true,

      -- Automatically jump forward to textobj, similar to targets.vim 
      lookahead = true,

      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ic"] = "@conditional.inner",
        ["ac"] =  "@conditional.outer",
        ["il"] =  "@loop.inner",
        ["al"] =  "@loop.outer",
        ["ib"] =  "@block.inner",
        ["ab"] =  "@block.outer",
        ["aC"] =  "@call.outer",
        ["ip"] =  "@parameter.inner",


        -- Or you can define your own textobjects like this
      },
    },
  },
  }
	
require'nvim-web-devicons'.get_icons()


  require("indent_blankline").setup {
	  char = "|",
	  indent_blankline_use_treesitter = true,
	  indent_blankline_strict_tabs = true,
      buftype_exclude = {"terminal"}
    }

require'lualine'.setup {
  options = {
    theme = 'palenight',
    component_separators = {'', ''},
    section_separators = {'', ''},
    disabled_filetypes = {}
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch'},
    lualine_c = {'filename'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    --lualine_a = {},
	lualine_x = {}
    --lualine_b = {},
    --lualine_y = {},
    --lualine_z = {}
  },
  tabline = {},
  extensions = {}
}


local nvim_lsp = require('lspconfig')
local coq = require "coq" -- add this

require("coq_3p")
{
  { src = "copilot", short_name = "COP", accept_key = "<c-f>" }
}

		
nvim_lsp.clangd.setup{coq.lsp_ensure_capabilities({ "$HOME/.local/share/nvim/lsp_servers/clangd/clangd","--query-driver=libstdc++","  --query-driver=/usr/bin/clang++", "--background-index" ,"-j=10"})
}
require('telescope').setup{
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
	  '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case'
    },
    prompt_prefix = "> ",
    selection_caret = "> ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "horizontal",
    layout_config = {
      horizontal = {
        mirror = false,
      },
      vertical = {
        mirror = false,
      },
    },
    file_sorter =  require'telescope.sorters'.get_fuzzy_file,
    file_ignore_patterns = {},
    generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
    winblend = 0,
    brder = {},
    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    color_devicons = true,
    use_less = true,
    path_display = {},
    set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
    file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
    grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
    qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,

    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker
  }


}

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
require('lsp_signature').on_attach()
local on_attach = function(client, bufnr)
	require "lsp_signature".on_attach({
      bind = true, -- This is mandatory, otherwise border config won't get registered.
      handler_opts = {
        border = "single"
      }
    }, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }
   -- See `:help vim.lsp.*` for documentation on any of the below functions
   buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
 -- buf_se_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
--  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end
-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches


local servers = { 'clangd', 'tsserver'}
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }
end

local sumneko_root_path = ""
local sumneko_binary = ""
USER = vim.fn.expand('$USER')

if vim.fn.has("mac") == 1 then
    sumneko_root_path = "/Users/" .. USER .. "/.config/nvim/lua-language-server"
    sumneko_binary = "/Users/" .. USER .. "/.config/nvim/lua-language-server/bin/macOS/lua-language-server"
elseif vim.fn.has("unix") == 1 then
    sumneko_root_path = "/home/" .. USER .. "/.config/nvim/lua-language-server"
    sumneko_binary = "/home/" .. USER .. "/.config/nvim/lua-language-server/bin/Linux/lua-language-server"
else
    print("Unsupported system for sumneko")
end

require'lspconfig'.sumneko_lua.setup {
    cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server hich version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
                -- Setup your lua path
                path = vim.split(package.path, ';')
				
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = {'vim'}
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = {[vim.fn.expand('$VIMRUNTIME/lua')] = true, [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true}
            }
        }
    }
}
require'FTerm'.setup({
    dimensions  = {
        height = 1,

        width = 1,
        x = 0.5,
        y = 25
    },
    border = 'single' -- or 'double'
})
 require "lsp_signature".setup({
    bind = true, -- This is mandatory, otherwise border config won't get registered.
    handler_opts = {
      border = "single"
    }
  })
  
require("telescope").load_extension('harpoon')
require("harpoon").setup({ ... })
