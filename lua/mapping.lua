 vim.api.nvim_set_keymap('n', '<Leader>', '<CR>', {noremap = true})
 vim.api.nvim_set_keymap('n', ';', ':', {noremap = true})
 vim.api.nvim_set_keymap('n', ':', ';', {noremap = true})
 vim.api.nvim_set_keymap('n', '{<CR>', '{<CR>}<Esc>O', {noremap = true})
 vim.api.nvim_set_keymap('n', '{<CR>', '{<CR>}<Esc>O', {noremap = true})
 vim.api.nvim_set_keymap('i', '{<CR>', '{<CR>}<Esc>O', {noremap = true})
 vim.api.nvim_set_keymap('n', '<Leader>y', '"*y', {noremap = true})
 vim.api.nvim_set_keymap('n', '<Leader>p', '+p', {noremap = true})
 vim.api.nvim_set_keymap('n', '<Leader>P', '*p', {noremap = true})
 vim.api.nvim_set_keymap('n', 'gb', 'gD', {noremap = true})
 vim.api.nvim_set_keymap('n', 'gb', 'gD', {noremap = true})
 vim.api.nvim_set_keymap('n', '<Leader>k', ':Telescope treesitter<Cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'tt', ':lua require("harpoon.ui").toggle_quick_menu()<Cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<Leader>i', ':Telescope  <Cr>', {noremap = true})
 vim.api.nvim_set_keymap('n', '<Leader>d', ':!norminette % | grep -w "Error" | head -n 9 ', {noremap = true})
vim.cmd[[tnoremap <Esc> <C-\><C-n>

nnoremap <Leader>j   :lua require("FTerm").toggle() <CR>
tnoremap <Leader>j  <Esc> <C-\><C-n> :lua require("FTerm").toggle() <CR>
nnoremap <leader>1 :Stdheader <CR>
nnoremap <leader>6 :hi Normal guibg=NONE ctermbg=NONE <CR>
nnoremap <leader>d :!norminette % <CR>
nnoremap <leader>5 :!make <CR>
nnoremap <silent> gs :Lspsaga signature_help<CR>
nnoremap <C-J> :lua require("harpoon.ui").nav_next()<CR>
nnoremap <C-K> :lua require("harpoon.ui").nav_prev()<CR>
nnoremap th :lua require("harpoon.mark").add_file() <CR>
nnoremap tg :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap tc :Telescope harpoon marks<CR>
]]

--noremap <Leader>f :Files <CR>
--noremap <Leader>g :Buffers <CR>

