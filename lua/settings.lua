local set = vim.opt
set.syntax = "on"
set.number = true
set.shiftwidth = 4
set.softtabstop = 4
vim.g.filetype = "filetype-plugin-indent-on"
--migh:t break 
vim.g.mapleader = ' '
set.tabstop = 4
vim.env.USER = "jemartel"
vim.env.MAIL = "jemartel@student.42quebec.com"
